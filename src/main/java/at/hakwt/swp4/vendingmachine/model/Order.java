package at.hakwt.swp4.vendingmachine.model;

public class Order {

    private String drinkName;
    private int amount;

    public Order(String drinkName, int amount) {
        this.drinkName = drinkName;
        this.amount = amount;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public int getAmount() {
        return amount;
    }
}
